package exchange

import (
	"github.com/cenkalti/backoff/v4"
	"github.com/streadway/amqp"
	"log"
)

type Consumer struct {
	uri          string
	exchangeName string
	conn         *amqp.Connection
	ch           *amqp.Channel
	queue        *amqp.Queue
	isConnected  bool
	rChannel     chan []byte
}

func NewBroadcastConsumer(uri, exchangeName string, rChannel chan []byte) (*Consumer, error) {
	brodCons := &Consumer{}

	brodCons.uri = uri
	brodCons.exchangeName = exchangeName
	brodCons.rChannel = rChannel

	err := brodCons.connect(false)
	if err != nil {
		log.Println("[error]", err)
		return nil, err
	}
	//handle with errors
	go func() {
		for {
			er := <-brodCons.conn.NotifyClose(make(chan *amqp.Error))
			brodCons.retryReconnect(er)
		}
	}()
	return brodCons, nil
}

func (brodCons *Consumer) connect(reconnect bool) (err error) {
	done := make(chan error)
	if reconnect {
		if brodCons.ch != nil {
			brodCons.ch.Close()
		}
		if brodCons.conn != nil {
			brodCons.conn.Close()
		}
	}
	brodCons.conn, err = amqp.Dial(brodCons.uri)

	if err != nil {
		log.Println("[error]", "Failed to connect to RabbitMQ:", err)
		return err
	}
	// defer conn.Close()
	brodCons.ch, err = brodCons.conn.Channel()
	if err != nil {
		log.Println("[error]", "Failed to open a channel:", err)
		return err
	}

	// defer ch.Close()
	err = brodCons.ch.ExchangeDeclare(
		brodCons.exchangeName, // name
		amqp.ExchangeFanout,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Println("[error]", "Failed to declare an exchange: %s", err)
		return err
	}

	q, err := brodCons.ch.QueueDeclare(
		"",
		false,
		true,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Println("[error]", "Failed to declare a queue:", err)
		return err
	}
	brodCons.queue = &q

	err = brodCons.ch.QueueBind(
		brodCons.queue.Name,
		"",
		brodCons.exchangeName,
		false,
		nil)
	if err != nil {
		log.Println("[error]", "Failed to bind a queue: %s", err)
		return err
	}

	deliveries, err := brodCons.ch.Consume(
		brodCons.queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Println("[error]", "Failed to register a consumer:", err)
		return err
	}
	log.Println("consumer started with success")
	go brodCons.handle(deliveries, done)

	brodCons.isConnected = true
	return
}

//TODO: check date
func (brodCons *Consumer) handle(deliveries <-chan amqp.Delivery, done chan error) {
	for d := range deliveries {
		brodCons.rChannel <- d.Body
	}

	log.Println("[error]", "handle: deliveries channel closed")
	done <- nil
}

func (brodCons *Consumer) retryReconnect(amqpErr error) {
	log.Println("[error]", "connection lost", amqpErr)
	brodCons.isConnected = false

	b := backoff.NewExponentialBackOff()
	ticker := backoff.NewTicker(b)
	for range ticker.C {
		log.Print("[error]", "try reconnect...")
		if err := brodCons.connect(true); err != nil {
			log.Println("[error]", "i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}
