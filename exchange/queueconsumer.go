package exchange

import (
	"github.com/cenkalti/backoff/v4"
	"github.com/streadway/amqp"
	"log"
)

type QueueConsumer struct {
	uri          string
	exchangeName string
	queueName    string
	isConnected  bool

	conn  *amqp.Connection
	ch    *amqp.Channel
	queue *amqp.Queue

	rChannel chan []byte
}

func NewQueueConsumer(uri, exchangeName, queueName string, rChannel chan []byte) (queueCons *QueueConsumer, err error) {
	queueCons = &QueueConsumer{}

	queueCons.uri = uri
	queueCons.exchangeName = exchangeName
	queueCons.queueName = queueName
	queueCons.rChannel = rChannel

	if err := queueCons.connect(false); err != nil {
		return nil, err
	}

	//handle with errors
	go func() {
		for {
			er := <-queueCons.conn.NotifyClose(make(chan *amqp.Error))
			queueCons.retryReconnect(er)
		}
	}()
	return queueCons, nil
}

func (queueCons *QueueConsumer) connect(reconnect bool) (err error) {
	done := make(chan error)
	if reconnect {
		if queueCons.ch != nil {
			queueCons.ch.Close()
		}
		if queueCons.conn != nil {
			queueCons.conn.Close()
		}
	}
	queueCons.conn, err = amqp.Dial(queueCons.uri)
	if err != nil {
		log.Println("Failed to connect to RabbitMQ:", err)
		return err
	}
	queueCons.ch, err = queueCons.conn.Channel()
	if err != nil {
		log.Println("Failed to open a channel:", err)
		return err
	}

	err = queueCons.ch.ExchangeDeclare(
		queueCons.exchangeName, // name
		amqp.ExchangeFanout,    // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		log.Println("Failed to declare an exchange: %s", err)
		return err
	}

	q, err := queueCons.ch.QueueDeclare(
		queueCons.queueName, // name
		true,                // durable
		false,               // delete when unused
		false,               // exclusive
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		log.Println("Failed to declare a queue:", err)
		return err
	}
	queueCons.queue = &q

	err = queueCons.ch.QueueBind(
		queueCons.queue.Name,   // queue name
		"",                     // routing key
		queueCons.exchangeName, // exchange
		false,
		nil)
	if err != nil {
		log.Println("Failed to bind a queue: %s", err)
		return err
	}

	deliveries, err := queueCons.ch.Consume(
		queueCons.queue.Name, // queue
		"",                   // consumer
		true,                 // auto-ack
		false,                // exclusive
		false,                // no-local
		false,                // no-wait
		nil,                  // args
	)
	if err != nil {
		log.Println("Failed to register a consumer:", err)
		return err
	}

	log.Println("RabbitMQ connected")

	go queueCons.handle(deliveries, done)
	queueCons.isConnected = true
	return
}

//TODO: check date
func (queueCons *QueueConsumer) handle(deliveries <-chan amqp.Delivery, done chan error) {
	for d := range deliveries {
		queueCons.rChannel <- d.Body
	}

	log.Printf("handle: deliveries channel closed")
	done <- nil
}

func (queueCons *QueueConsumer) retryReconnect(amqpErr error) {
	log.Println("connection lost", amqpErr)
	queueCons.isConnected = false

	b := backoff.NewExponentialBackOff()
	ticker := backoff.NewTicker(b)
	for range ticker.C {
		log.Print("try reconnect...")
		if err := queueCons.connect(true); err != nil {
			log.Println("i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}
