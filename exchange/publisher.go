package exchange

import (
	"github.com/cenkalti/backoff/v4"
	"github.com/streadway/amqp"
	"log"
)

type Publisher struct {
	uri          string
	exchangeName string
	conn         *amqp.Connection
	ch           *amqp.Channel
	isConnected  bool
}

func NewPublisher(uri, exchangeName string) (*Publisher, error) {
	pub := &Publisher{}
	pub.uri = uri
	pub.exchangeName = exchangeName

	err := pub.connect(false)
	if err != nil {
		log.Println("[error]", err)
		return nil, err
	}

	//handle with errors
	go func() {
		for {
			pub.retryReconnect(<-pub.conn.NotifyClose(make(chan *amqp.Error)))
		}
	}()

	return pub, nil
}

func (pub *Publisher) connect(reconnect bool) error {
	var err error
	if reconnect {
		if pub.ch != nil {
			pub.ch.Close()
		}
		if pub.conn != nil {
			pub.conn.Close()
		}
	}

	pub.conn, err = amqp.Dial(pub.uri)
	if err != nil {
		log.Println("Failed to connect to RabbitMQ:", err.Error())
		return err
	}
	pub.ch, err = pub.conn.Channel()
	if err != nil {
		log.Println("Failed  to open a channel:", err.Error())
		return err
	}

	if !reconnect {
		err = pub.ch.ExchangeDeclare(
			pub.exchangeName,
			amqp.ExchangeFanout,
			true,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			log.Println("Failed declare an exchange:", err.Error())
			return err
		}
	}

	log.Println("Publisher started with success")
	return nil
}

func (pub *Publisher) retryReconnect(amqpErr *amqp.Error) {
	log.Println("[error]", "connection lost", amqpErr.Error())
	pub.isConnected = false

	b := backoff.NewExponentialBackOff()
	ticker := backoff.NewTicker(b)
	for range ticker.C {
		log.Print("[error]", "try reconnect...")
		if err := pub.connect(true); err != nil {
			log.Println("[error]", "i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}

//SendMessage Send fucking message
func (pub *Publisher) SendMessage(msg []byte) (err error) {
	err = pub.ch.Publish(
		pub.exchangeName,
		"",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        msg,
		},
	)
	return err
}
