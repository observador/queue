module bitbucket.org/observador/queue/exchange

go 1.13

require (
	github.com/cenkalti/backoff/v4 v4.0.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
)
