package directqueue

type Message struct {
	Queue   string
	Message []byte
	Error   chan error
}
type Queue struct {
	Name    string
	AutoAck bool // only for consumers
}
