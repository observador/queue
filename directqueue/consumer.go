package directqueue

import (
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"
)

type Consumer struct {
	uri             string
	queue           Queue
	connection      *amqp.Connection
	channel         *amqp.Channel
	wg              sync.WaitGroup
	ReceiverChannel chan amqp.Delivery
}

func NewConsumer(uri string, queue Queue) (ra *Consumer, err error) {
	ra = new(Consumer)
	ra.queue = queue
	ra.uri = uri
	ra.ReceiverChannel = make(chan amqp.Delivery)
	err = ra.connect(false)
	if err != nil {
		ra.retryReconnect(err)
	}

	go func() {
		for {
			ra.retryReconnect(<-ra.connection.NotifyClose(make(chan *amqp.Error)))
		}
	}()
	return ra, nil
}

func (cons *Consumer) retryReconnect(err error) {
	ticker := time.NewTicker(time.Second * 5)
	for range ticker.C {
		log.Print("try reconnect...")
		if err := cons.connect(true); err != nil {
			log.Println("i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}

func (cons *Consumer) connect(reconnect bool) (err error) {
	if reconnect {
		if cons.channel != nil {
			cons.channel.Close()
		}
		if cons.connection != nil {
			cons.connection.Close()
		}
	}
	cons.connection, err = amqp.Dial(cons.uri)
	if err != nil {
		log.Println("Dial:", err)
		return err
	}

	log.Printf("got Connection, getting Channel")

	cons.channel, err = cons.connection.Channel()
	if err != nil {
		log.Printf("Dial: %s", err)
		log.Printf("Channel: %s", err)
		return err
	}

	// create queue if not exists
	_, err = cons.channel.QueueDeclare(
		cons.queue.Name, // name
		true,            // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	if err != nil {
		return err
	}

	deliveries, err := cons.channel.Consume(
		cons.queue.Name,    // name
		"",                 // consumer
		cons.queue.AutoAck, // auto-ack
		false,              // exclusive
		false,              // no-local
		false,              // no-wait
		nil,                // args
	)
	if err != nil {
		log.Printf("Queue Consume: %s", err)
		return err
	}

	go cons.handle(deliveries)
	return nil
}

func (cons *Consumer) handle(deliveries <-chan amqp.Delivery) {
	for d := range deliveries {
		cons.ReceiverChannel <- d
	}
}
