package directqueue

import (
	"log"
	"time"

	"github.com/streadway/amqp"
)

type Publisher struct {
	uri          string
	topics       []Queue
	connection   *amqp.Connection
	channel      *amqp.Channel
	queues       map[string]*amqp.Queue
	SenderChanel chan Message
	isConnected  bool
}

func NewPublisher(uri string, topics []Queue) (pub *Publisher, err error) {

	senderChannel := make(chan Message, 1)
	pub = new(Publisher)
	pub.uri = uri
	pub.SenderChanel = senderChannel
	pub.topics = topics

	err = pub.connect(false)
	if err != nil {
		return pub, err
	}

	go func() {
		for {
			pub.retryReconnect(<-pub.connection.NotifyClose(make(chan *amqp.Error)))
		}
	}()

	go pub.postMessage(senderChannel)

	return pub, err
}

func (pub *Publisher) retryReconnect(amqpErr *amqp.Error) {
	log.Println("[error]", "connection lost", amqpErr.Error())
	pub.isConnected = false

	ticker := time.NewTicker(time.Second * 5)
	for range ticker.C {
		log.Print("[error]", "try reconnect...")
		if err := pub.connect(true); err != nil {
			log.Println("[error]", "i can't reconnect", err)
			continue
		}
		ticker.Stop()
		break
	}
}

func (pub *Publisher) connect(reconnect bool) (err error) {
	if reconnect {
		if pub.channel != nil {
			pub.channel.Close()
		}
		if pub.connection != nil {
			pub.connection.Close()
		}
	}

	log.Printf("dialing %s\n", pub.uri)
	pub.connection, err = amqp.Dial(pub.uri)
	if err != nil {
		log.Printf("error dialing: %v\n", err)
		return err
	}

	log.Printf("Got Connection, getting Channel\n")
	pub.channel, err = pub.connection.Channel()
	if err != nil {
		log.Fatalf("error connecting channel: %s\n", err)
		return err
	}

	err = pub.createQueues()
	if err != nil {
		return err
	}

	return nil
}

func (pub *Publisher) disconnect() {
	defer pub.connection.Close()
}

func (pub *Publisher) createQueues() (err error) {

	queues := make(map[string]*amqp.Queue)
	for _, t := range pub.topics {
		log.Print(t)
		q, err := pub.channel.QueueDeclare(
			t.Name, // name
			true,   // durable
			false,  // delete when unused
			false,  // exclusive
			false,  // no-wait
			nil,    // arguments
		)
		if err != nil {
			return err
		}
		queues[t.Name] = &q
	}
	pub.queues = queues
	return nil
}

func (pub *Publisher) postMessage(mm <-chan Message) {
	for {
		n, ok := <-mm
		if ok {
			err := pub.channel.Publish(
				"",      // exchange
				n.Queue, // routing key
				false,   // mandatory
				false,   // immediate
				amqp.Publishing{
					DeliveryMode: amqp.Persistent,
					ContentType:  "application/json",
					Body:         n.Message,
				},
			)
			if n.Error != nil {
				if err != nil {
					n.Error <- err
				} else {
					close(n.Error)
				}
			}
		}
	}
}
